WHERE TO START
--------------

The Athletic theme is a Sub Theme of the Fusion theme, so you'll need to follow the instructions below to get it installed and started.


INSTALLATION
------------

 1. Download Fusion from http://drupal.org/project/fusion

 2. Unpack the downloaded file, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

 3. Install the Skinr module: http://drupal.org/project/skinr
    Skinr makes Fusion even more powerful, giving you control over Fusion's 
    layout and style options in Drupal's interface. Download and install 
    this module like usual to get the most out of Fusion and you're new Athletic Theme.
	
	The Skinr styles have already been added in, but you will need to create a new rule.
	For this theme to work, please be sure to sure to use Skinr version 6.x-2.x-dev which is the latest dev version.
	Once you have it istalled, you will need to create a new rule called "Colors"; from there you will be able to assign colors to select areas.

	* How to install modules: http://drupal.org/node/70151

 4. Follow the instructions below to build your own Fusion subtheme.


Optional:

    MODIFYING TEMPLATE FILES:
    If you decide you want to modify any of the .tpl.php template files in the
    fusion_core folder, copy them to your subtheme's folder before making any 
    changes. Then rebuild the theme registry.

    For example, copy fusion_core/page.tpl.php to sunshine/page.tpl.php

